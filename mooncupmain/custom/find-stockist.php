<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Find a Stockist
 */

get_header();
if(isset($_GET['addressInput'])):
?>
<script>
$(document).ready(function(){
    $('#searchForm').submit();
});
    </script>
<?php
endif;

?>

<section class="find-stockist-page page-content primary" role="main" >
	<div class="container_boxed content_band--small">

		<div class="page-head">
			<?php
			the_field('1col_content_area');
			if($_COOKIE['location'] == 'DE' || $_GET['geocode'] == 'DE'){
			echo '<p>Sie k&ouml;nnen auch jetzt Ihre Mooncup einfach in <strong>jeder Apotheke in Deutschland bestellen.</strong><br /> Dabei geben Sie einem der Angestellten bitte die folgende PZN Nummer f&uuml;r die jeweilige Gr&ouml;&szlig;e:<strong> Gr&ouml;&szlig;e A: 06709194 und Gr&ouml;&szlig;e B: 06709188</strong></p>';
			}
			?>

		</div>

		<?php
		if ( have_posts() ) : the_post();

			get_template_part( 'loop' ); ?>

			<aside class="post-aside"><?php

			wp_link_pages(
					array(
							'before'           => '<div class="linked-page-nav"><p>' . sprintf( __( '<em>%s</em> is separated in multiple parts:', 'mooncupmain' ), get_the_title() ) . '<br />',
							'after'            => '</p></div>',
							'next_or_number'   => 'number',
							'separator'        => ' ',
							'pagelink'         => __( '&raquo; Part %', 'mooncupmain' ),
					)
			); ?>

			<?php
			if ( comments_open() || get_comments_number() > 0 ) :
				comments_template( '', true );
			endif;
			?>

			</aside><?php

		else :

			get_template_part( 'loop', 'empty' );

		endif;
		?>
	</div>
</section>



<?php get_footer(); ?>
<script>

	function getQueryVariable(variable)
	{

		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
			if(pair[0] == variable){return pair[1];}
		}
		return(false);
	}
	if(window.location.search.substring(1)) {
		var myParam = getQueryVariable("addressInput");
		var elem = document.getElementById("addressInput");
		elem.value = myParam;
		if (document.referrer != window.parent.location) {
			cslmap.searchLocations();
		}
	}

</script>