<?php
/**
 * Country selector form
 *
 * @author 		oscargare
 * @version     1.5.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! function_exists( 'wcpbc_manual_country_script' ) ) {

	function wcpbc_manual_country_script() {
		?>
		<script type="text/javascript">
			jQuery( document ).ready( function( $ ){
				$('.wcpbc-widget-country-selecting').on('change', 'select.country', function(){
					$(this).closest('form').submit();
				} );
			} );	
		</script>
		<?php
	}
}
$regions = get_option( 'wc_price_based_country_regions', array() );	

add_action( 'wp_print_footer_scripts', 'wcpbc_manual_country_script' );
$_COOKIE['location'] = $selected_country;
if ( $countries ) : ?>
		
	<form method="post" class="wcpbc-widget-country-selecting" action="/buy-the-mooncup/">		
		<select class="country" name="wcpbc-manual-country">
			<?php foreach ($countries as $key => $value) : ?>
            <?php if($key == 'GB' || $key == 'US'):?>
				<option value="<?php echo $key?>" <?php echo selected($key, $selected_country ); ?> ><?php echo $value; ?></option>
                <?php endif;?>
			<?php endforeach; ?>
            <?php foreach ($countries as $key => $value) : ?>
            <?php if($key != 'GB' && $key != 'US' && !in_array($key,$regions['other']['countries'])):?>
				<option value="<?php echo $key?>" <?php echo selected($key, $selected_country ); ?> ><?php echo $value; ?></option>
                <?php endif;?>
			<?php endforeach; ?>
		</select>					
	</form>			

<?php endif; ?>